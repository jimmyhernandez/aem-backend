package com.rest101.core;

import com.adobe.cq.sightly.WCMUsePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;

/**
 * Created by jimmy on 2017-06-08.
 */
public class HeroRestComponent extends WCMUsePojo {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    InfoBean info = null;

    @Override
    public void activate() throws Exception {
        logger.info("Start WCMUSEPojo");
        logger.error("Start WCMUSEPojo");

        info = new InfoBean();
        StringBuilder result = new StringBuilder();
        String props = null;

        Node currentNode = getResource().adaptTo(Node.class);


        if(currentNode.hasProperty("tocity")) {
            result.append("To City: " + currentNode.getProperty("./tocity").getString());
        }

        if(currentNode.hasProperty("fromcity")) {
            result.append("From City: " + currentNode.getProperty("./fromcity").getString());
        }

        info.setMessage(result.toString());
    }

    public InfoBean getInfo() {
        return this.info;
    }

    public class InfoBean {
        public String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
